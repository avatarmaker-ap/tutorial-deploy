package com.belajar.app.controller;

import java.util.List;

import com.belajar.app.PersonRepository;
import com.belajar.app.model.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    @Autowired
    PersonRepository repo;

    @GetMapping("/persons")
    public List<Person> getPersons() {
        return repo.findAll();
    }

    @PostMapping("/persons")
    public Person newPerson(@RequestBody Person person) {
        return repo.save(person);
    }
}